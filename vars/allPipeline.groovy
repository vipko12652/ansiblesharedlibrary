def call(body) {
  def pipelineParams= [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = pipelineParams
  body()


  properties([disableConcurrentBuilds()])

  pipeline {
      agent { label "make" }
//    agent { docker { image 'python:3.6' } }
      options {
          buildDiscarder(logRotator(numToKeepStr: '10', artifactNumToKeepStr: '10'))
          timestamps()
        }
      stages {

          stage('Test') {
              steps{
                  sh 'make test'
                }
            }

          stage ('getDockerfile') {
              steps {
                  script {
                      def tmpFile = libraryResource 'Dockerfile'
                      writeFile file: 'Dockerfile', text: tmpFile
                  }
              }
          }
          stage('docker-build') {
              steps{
                  sh 'make docker-build'
                }
            
            }

          stage('docker-pull') {
              steps{
                  withCredentials([usernamePassword(credentialsId: 'vipko_docker', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                      sh 'docker login --username $USERNAME --password $PASSWORD'
                      sh 'docker push vipko12652/sdr-srv:latest'
                    }
                }
            
            }
        }
    }
}